Proof of concept to demonstrate ArcGIS capabilities and possible future use as a feature of the
Availability Management Controller application.

- This is a React 'app' in a very loose sense of the word, as most of the functionality involves
  interfacing with the ArcGIS library.
- Uses Node.js, specifically v16.15.1, and npm v8.11.0.
- Made with create-react-app
