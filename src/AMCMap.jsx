import React, { useRef } from "react";
import { useCreateMap } from "./EsriController";
import CountrySelector from "./CountrySelector";


const AMCMap = () => {
  const mapRef = useRef(null);
  useCreateMap(mapRef);
  return (
    <div className="mapContainer">
      <CountrySelector />
      <div className="map-view" ref={mapRef}>
        <div
          id="clear-selection"
          className="esri-widget esri-widget--button esri-widget esri-interactive"
          title="Clear selection"
        >
          <span className="esri-icon-erase"></span>
        </div>
        <div
          id="select-by-polygon"
          className="esri-widget esri-widget--button esri-widget esri-interactive"
          title="Select features by polygon points"
        >
          <span className="esri-icon-lasso"></span>
        </div>
      </div>
      <div className="tableContainer">
        <div id="tableDiv"></div>
      </div>
    </div>
  );
};

export default AMCMap;
