import "./App.css";
import AMCMap from "./AMCMap";

function App() {
  return (
    <div className="App">
      <AMCMap />
    </div>
  );
}

export default App;
