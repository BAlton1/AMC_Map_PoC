import React from "react";
import CountryCodes from "./data/DubiousCountryCodes.json";

class CountrySelector extends React.Component {
  render() {
    const countrySelectionOptions = CountryCodes.codes.map((currentCode) => {
      return (
        <option id={currentCode.code} key={currentCode.code}>
          {currentCode.name}
        </option>
      );
    });

    return (
      <div className="esri-widget" id="filterDiv">
        <h2>Search by Country:</h2>
        <select className="esri-widget" id="countrySelector" size="6">
          {countrySelectionOptions}
        </select>
        <br />
        <br />
        <button className="esri-widget" id="filterBtn">
          Search
        </button>
        <button className="esri-widget" id="resetBtn">
          Reset
        </button>
        <br />
        <h4 id="searchResultNumber"></h4>
      </div>
    );
  }
}

export default CountrySelector;
