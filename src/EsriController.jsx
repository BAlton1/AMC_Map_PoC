import React from "react";
import Map from "@arcgis/core/Map";
import MapView from "@arcgis/core/views/MapView";
import Graphic from "@arcgis/core/Graphic";
import GraphicsLayer from "@arcgis/core/layers/GraphicsLayer";
import FeatureLayer from "@arcgis/core/layers/FeatureLayer";
import FeatureTable from "@arcgis/core/widgets/FeatureTable";
import esriConfig from "@arcgis/core/config.js";
import SketchViewModel from "@arcgis/core/widgets/Sketch/SketchViewModel";
import Search from "@arcgis/core/widgets/Search";
import * as geometryEngineAsync from "@arcgis/core/geometry/geometryEngineAsync";
import FeatureFilter from "@arcgis/core/layers/support/FeatureFilter";
import FeatureEffect from "@arcgis/core/layers/support/FeatureEffect";
import Query from "@arcgis/core/rest/support/Query";
import FCPData from "./data/testFcpData.json";

const ARC_GIS_API_KEY =
  "AAPK18ba479c17634772a141f0b01770bfe0wznSrZBnSaXBR91BRFxhQIjCTAl9yAEvP3MnPtbBzxnYtoj-KLn67JYaOKjzvPNf";
const ARC_GIS_BASEMAP_TYPE = "arcgis-community";
const MAP_CENTER = [0, 0];
const MAP_MIN_ZOOM = 1;
const MAP_MAX_ZOOM = 16;
const MAP_POINT_POPUP_TEMPLATE = {
  title: "{Church_Name}",
  content: "FCP ID: <b>{ Church_ID }</b>, Current Status: <b>{ Status }</b>",
};
let churchDataLayer;

/**
 * Creates an array of Graphics objects from a given JSON data file.
 * @returns the array of Graphics objects
 */
function createChurchGraphics() {
  let graphics = [];
  FCPData.forEach((currentChurch) => {
    if (
      currentChurch.Longitude !== null &&
      currentChurch.Latitude !== null &&
      currentChurch.Church_ID !== null &&
      currentChurch.Church_ID !== "" &&
      currentChurch.Church_Name !== null &&
      currentChurch.Church_Name !== "" &&
      currentChurch.Status !== null &&
      currentChurch.Status !== "" &&
      currentChurch.SF_ID !== null &&
      currentChurch.SF_ID !== ""
    ) {
      let point = {
        type: "point",
        latitude: currentChurch.Latitude,
        longitude: currentChurch.Longitude,
      };
      let currentGraphic = new Graphic({ geometry: point });
      currentGraphic.attributes = {
        SF_ID: currentChurch.SF_ID,
        Church_ID: currentChurch.Church_ID,
        Status: currentChurch.Status,
        Church_Name: currentChurch.Church_Name,
      };
      graphics.push(currentGraphic);
    }
  });
  return graphics;
}

/**
 * Handles the setup for the feature layer
 * @returns the populated FeatureLayer
 */
function createChurchFeatureLayer() {
  let churchGraphics = createChurchGraphics();
  let commonSymbolAttributes = {
    type: "simple-marker",
    size: "8px",
  };
  const featureLayer = new FeatureLayer({
    geometryType: "point",
    source: churchGraphics,
    fields: [
      {
        name: "SF_ID",
        alias: "Salesforce ID",
        type: "string",
      },
      {
        name: "Church_ID",
        alias: "Church ID",
        type: "string",
        editable: false,
      },
      {
        name: "Church_Name",
        alias: "Church Name",
        type: "string",
        editable: false,
      },
      {
        name: "Status",
        alias: "Status",
        type: "string",
        editable: false,
      },
    ],
    renderer: {
      type: "unique-value",
      field: "Status",
      uniqueValueInfos: [
        {
          value: "Active",
          symbol: {
            ...commonSymbolAttributes,
            color: "#7dbd2a",
          },
        },
        {
          value: "Draft",
          symbol: {
            ...commonSymbolAttributes,
            color: "#ffffff",
          },
        },
        {
          value: "Phase Out",
          symbol: {
            ...commonSymbolAttributes,
            color: "#7c7c7c",
          },
        },
        {
          value: "Suspended",
          symbol: {
            ...commonSymbolAttributes,
            color: "#f8de00",
          },
        },
        {
          value: "Transitioned",
          symbol: {
            ...commonSymbolAttributes,
            color: "#cc0000",
          },
        },
      ],
    },
    objectIdField: "OBJECTID",
    popupTemplate: MAP_POINT_POPUP_TEMPLATE,
  });
  return featureLayer;
}

/**
 * Creates a new GraphicsLayer object
 * @returns the new GraphicsLayer
 */
function createGraphicsLayer() {
  const polygonGraphicsLayer = new GraphicsLayer();
  return polygonGraphicsLayer;
}

/**
 * Creates a new Map object using the specified layer.
 * @param featureLayer the layer of features to be added to the map
 * @returns the map
 */
function createMap(featureLayer) {
  const map = new Map({
    basemap: ARC_GIS_BASEMAP_TYPE,
    layers: featureLayer,
  });
  return map;
}

/**
 * Creates a new MapView object from the specified Map and container and
 * sets some properties for the MapView.
 * @param map the map
 * @param mapContainer the page container that will hold the MapView
 * @returns the MapView
 */
function createMapView(map, mapContainer) {
  const mapView = new MapView({
    map: map,
    zoom: MAP_MIN_ZOOM,
    container: mapContainer,
    center: MAP_CENTER,
    constraints: {
      minZoom: MAP_MIN_ZOOM,
      maxZoom: MAP_MAX_ZOOM,
    },
  });
  return mapView;
}

/**
 * Creates a new FeatureTable that lists the churches included in the current Map,
 * as well as some important, relevant info.
 * @param mapView the mapView associated with features shown in the table.
 * @param dataLayer the data layer representing the churches shown in the MapView.
 * @returns the feature table
 */
function createFeatureDataTable(mapView, dataLayer) {
  const featureTable = new FeatureTable({
    view: mapView,
    layer: dataLayer,
    editingEnabled: false,
    fieldConfigs: [
      {
        name: "Church_ID",
        label: "Church ID",
      },
      {
        name: "Church_Name",
        label: "Church Name",
      },
      {
        name: "Status",
        label: "Current Status",
      },
    ],
    container: document.getElementById("tableDiv"),
  });
  return featureTable;
}

/**
 * Creates a new SketchViewModel object that handles the drawing/selecting
 * capability of the map.
 * @param mapView the MapView that will contain the sketch viewmodel.
 * @param graphicsLayer the layer which holds the sketch viewmodel's drawings.
 * @returns the sketch viewmodel
 */
function createSketchViewModel(mapView, graphicsLayer) {
  const sketchViewModel = new SketchViewModel({
    view: mapView,
    layer: graphicsLayer,
  });
  return sketchViewModel;
}

/**
 * A generic error handler that outputs to the console. Nothing fancy.
 * @param error the error caught by the caller.
 */
function handleError(error) {
  console.log("Oops: ", error.message);
}

/**
 * Checks to see if any church locations lie inside the polygon.
 * @param selectionArea the user-drawn area selection
 * @param dataPointLayerView the collection of data points
 *        shown in both the map and the feature table
 * @param featureTable the table displaying church data
 */
function selectChurches(selectionArea, featureTable) {
  featureTable.clearSelection();
  const query = {
    geometry: selectionArea,
    outFields: ["*"],
  };
  churchDataLayer
    .queryFeatures(query)
    .then((results) => {
      if (results.features.length > 0) {
        featureTable.filterGeometry = selectionArea;
        featureTable.selectRows(results.features);
      }
    })
    .catch(handleError);
}

/**
 * Searches through the data source to locate churches in the country
 * specified in the CountrySelector. If a country is found, the MapView
 * window is zoomed to the extent of the collection of churches and all
 * other points are blurred out.
 *
 * @param mapView the MapView
 */
function handleSearchByCountry(mapView) {
  let countrySelector = document.getElementById("countrySelector");
  let selectedCountry = countrySelector.options[countrySelector.selectedIndex];
  if (selectedCountry !== undefined) {
    const sqlWhere =
      "Church_ID LIKE '" + selectedCountry.getAttribute("id") + "%'";
    const query = new Query({
      returnGeometry: true,
      where: sqlWhere,
      outFields: ["*"],
    });
    churchDataLayer
      .queryExtent(query)
      .then((results) => {
        let resultsLabel = document.getElementById("searchResultNumber");
        resultsLabel.textContent =
          "Churches in " + selectedCountry.value + ": " + results.count;
        if (results.count > 0) {
          churchDataLayer.featureEffect = new FeatureEffect({
            filter: new FeatureFilter({
              where: sqlWhere,
            }),
            excludedEffect: "blur(5px) grayscale(90%) opacity(40%)",
          });
          mapView.goTo(results.extent).catch(handleError);
        }
      })
      .catch((error) => {
        handleError(error);
      });
  }
}

/**
 * Resets the mapView window to the original center and zoom level,
 * updates the query result message, and clears the blurring filter.
 * @param mapView the mapView
 */
function handleResetView(mapView) {
  mapView
    .goTo({
      center: MAP_CENTER,
      zoom: MAP_MIN_ZOOM,
    })
    .catch(handleError);
  let resultsLabel = document.getElementById("searchResultNumber");
  resultsLabel.textContent = "";
  churchDataLayer.featureEffect = undefined;
}

/**
 * Performs all necessary functionality to create the map control. I am
 * aware of how painfully massive this method is; I just can't factor out any more
 * private helpers without passing parameters all over the place :(
 * @param mapRef the reference to the component's container element
 */
export function useCreateMap(mapRef) {
  React.useEffect(() => {
    let mapView;

    const setupMapControl = (mapRef) => {
      esriConfig.apiKey = ARC_GIS_API_KEY;

      const churchFeatureLayer = createChurchFeatureLayer();
      churchDataLayer = churchFeatureLayer;
      const polygonGraphicsLayer = createGraphicsLayer();
      const map = createMap(churchFeatureLayer);
      map.add(polygonGraphicsLayer);
      mapView = createMapView(map, mapRef.current);

      mapView.when(() => {
        const featureTable = createFeatureDataTable(
          mapView,
          churchFeatureLayer
        );
        const sketchVM = createSketchViewModel(mapView, polygonGraphicsLayer);

        mapView.ui.add("select-by-polygon", "top-left");
        const selectButton = document.getElementById("select-by-polygon");
        selectButton.addEventListener("click", () => {
          mapView.popup.close();
          sketchVM.create("polygon");
        });

        mapView.ui.add("clear-selection", "top-left");
        const clearButton = document.getElementById("clear-selection");
        clearButton.addEventListener("click", () => {
          featureTable.clearSelection();
          featureTable.filterGeometry = null;
          polygonGraphicsLayer.removeAll();
        });

        const searchWidget = new Search({
          view: mapView,
          locationEnabled: false,
          suggestionsEnabled: false,
          includeDefaultSources: false,
          autoSelect: true,
          sources: [
            {
              layer: churchFeatureLayer,
              searchFields: ["Church_ID"],
              exactMatch: false,
              name: "FCP ID",
              placeholder: "Search for an FCP here...",
            },
          ],
        });
        mapView.ui.add(searchWidget, "top-right");

        const filterBtn = document.getElementById("filterBtn");
        filterBtn.addEventListener("click", () => {
          handleSearchByCountry(mapView);
        });

        const resetBtn = document.getElementById("resetBtn");
        resetBtn.addEventListener("click", () => {
          handleResetView(mapView);
        });

        sketchVM.on("create", async (event) => {
          if (event.state === "complete") {
            const geometries = polygonGraphicsLayer.graphics.map(function (
              graphic
            ) {
              return graphic.geometry;
            });
            const totalSelectedArea = await geometryEngineAsync.union(
              geometries.toArray()
            );
            selectChurches(totalSelectedArea, featureTable);
          }
        });
      });
    };
    setupMapControl(mapRef);
    return () => {
      mapView?.destroy();
    };
  }, [mapRef]);
}
